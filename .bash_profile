
export PATH="$HOME/Applications/bin:$PATH";

# Load extras
source ~/.bash_prompt
source ~/.git_completion

# Load Nix
. ~/.nix-profile/etc/profile.d/nix.sh

# Autocorrect typos in path names when using `cd`
shopt -s cdspell;

# Enable some Bash 4 features when possible:
# * `autocd`, e.g. `**/qux` will enter `./foo/bar/baz/qux`
# * Recursive globbing, e.g. `echo **/*.txt`
for option in autocd globstar; do
  shopt -s "$option" 2> /dev/null;
done;

# Set default editor
if hash code 2>/dev/null; then
  export EDITOR="code";
  git config --global core.editor "code -w"
else
  export EDITOR="nano";
fi

# Set git difftool
if hash ksdiff 2>/dev/null; then
  git config --global merge.tool ksdiff
  git config --global mergetool.ksdiff.cmd 'ksdiff --merge --output "$MERGED" --base "$BASE" -- "$LOCAL" --snapshot "$REMOTE" --snapshot'
  git config --global diff.tool ksdiff
  git config --global difftool.ksdiff.cmd 'ksdiff --partial-changeset --relative-path "$MERGED" -- "$LOCAL" "$REMOTE"'
fi

# Make some commands not show up in history
export HISTIGNORE="ls:cd:cd -:pwd:exit:date:* --help";

# Highlight section titles in manual pages
export LESS_TERMCAP_md="${yellow}";

# Don't clear the screen after quitting a manual page
export MANPAGER="less -X";

# Always enable colored `grep` output
export GREP_OPTIONS="--color=auto";

# Functions

function bb() {
  open "https://bitbucket.org/`git remote -v | grep bitbucket | head -n 1 | sed -e 's/.*:\(.*\)\/\(.*\)\.git.*/\1\/\2/'`"
}

function bbpr() {
  open "https://bitbucket.org/`git remote -v | grep bitbucket | head -n 1 | sed -e 's/.*:\(.*\)\/\(.*\)\.git.*/\1\/\2/'`/pull-request/new"
}

function gh() {
  REPO=`git remote -v | grep github.com | head -n 1 | sed -e 's/.*:\([^.]*\).*/\1/'`
  open "https://github.com/$REPO/"
}

function ghpr() {
  BRANCH=`git symbolic-ref --quiet --short HEAD`
  REPO=`git remote -v | grep github.com | head -n 1 | sed -e 's/.*:\([^.]*\).*/\1/'`
  open "https://github.com/$REPO/compare/master...$BRANCH"
}

function openwith {
  /System/Library/Frameworks/CoreServices.framework/Frameworks/LaunchServices.framework/Support/lsregister -kill -r -domain local -domain system -domain user
}

function dnsflush {
  sudo discoveryutil udnsflushcaches
}

function colors() {
  for (( i = 0; i < 256; i++ )); do
    echo "$(tput setaf $i)tput setaf $i $(tput sgr0)";
  done
}

function datauri() {
  local mimeType=$(file -b --mime-type "$1");
  if [[ $mimeType == text/* ]]; then
    mimeType="${mimeType};charset=utf-8";
  fi
  echo "data:${mimeType};base64,$(openssl base64 -in "$1" | tr -d '\n')";
}

function sshmod() {
  chmod 600 ~/.ssh/*
  chmod 644 ~/.ssh/known_hosts
  chmod 755 ~/.ssh/
}

function gitprompt() {
  echo "[+] Uncommited changes"
  echo "[!] Unstaged changes"
  echo "[?] Untracked files"
  echo "[$] Stashed files"
}
