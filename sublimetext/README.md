# Sublime Text 3

http://www.sublimetext.com/3

Package Control

- https://sublime.wbond.net

Packages

- Theme - Spacegray
- EditorConfig
- GitConfig
- GitGutter
- GitSavvy
- Babel
- Better CoffeeScript
