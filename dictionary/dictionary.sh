cd ~/Library/Dictionaries/

__DICTIONARY_INSTALLED=false

__DICTIONARY="English-Français.dictionary"
if [ ! -d "$__DICTIONARY" ]; then
  svn export https://github.com/afischer/osx-dictionaries/trunk/english/English-Français.dictionary
  __DICTIONARY_INSTALLED=true
fi

__DICTIONARY="Français-English.dictionary"
if [ ! -d "$__DICTIONARY" ]; then
  svn export https://github.com/afischer/osx-dictionaries/trunk/french/Français-English.dictionary
  __DICTIONARY_INSTALLED=true
fi

if [ "$__DICTIONARY_INSTALLED" = true ]; then
  open -a Dictionary
fi
