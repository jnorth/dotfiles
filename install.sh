#!/usr/bin/env bash

# Variables
DOTFILES_GIT_USERNAME="$(id -F)"
DOTFILES_GIT_EMAIL="$(git config --global user.email)"
DOTFILES_PATH=$(cd `dirname "${BASH_SOURCE[0]}"` && pwd)

# Make sure we're running in the dotfile project folder
cd $DOTFILES_PATH

# Copy dotfiles
rsync \
  --exclude ".git/" \
  --exclude ".DS_Store" \
  -avh --no-perms .[^.]* ~;

# Nix
curl https://nixos.org/nix/install | sh
mkdir -p ~/.config/nixpkgs
echo "{ allowUnfree = true; }" > ~/.config/nixpkgs/config.nix
. ~/.nix-profile/etc/profile.d/nix.sh

# Nix packages
nix-env -i \
  vscode \
  nodejs

# Init profile
source ~/.bash_profile

# Git
read -p "Enter git user name: [$DOTFILES_GIT_USERNAME] " __GIT_USERNAME
if [ -z "$__GIT_USERNAME" ]; then
  __GIT_USERNAME="$DOTFILES_GIT_USERNAME"
fi
git config --global user.name "$__GIT_USERNAME"

read -p "Enter git email address: [$DOTFILES_GIT_EMAIL] " __GIT_USEREMAIL
if [ -z "$__GIT_USEREMAIL" ]; then
  __GIT_USEREMAIL="$DOTFILES_GIT_EMAIL"
fi
git config --global user.email $__GIT_USEREMAIL

# Fonts
unzip fonts/Input.zip -d ~/Library/Fonts/
git clone --depth 1 https://github.com/google/fonts.git ~/Library/Fonts/Google\ Fonts

# Install VS Code preferences
mkdir -p ~/Library/Application\ Support/Code/User
cp vscode/*.json ~/Library/Application\ Support/Code/User/

# Install VS Code extensions
code --install-extension ms-vsliveshare.vsliveshare
code --install-extension teabyii.ayu

# Terminal
open terminal/ToyChestCustom.terminal

# Dictionary
source dictionary/dictionary.sh

# OSX
cd $DOTFILES_PATH
source osx/defaults.sh
